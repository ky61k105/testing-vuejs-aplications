import flushPromises from 'flush-promises'

import { shallowMount } from '@vue/test-utils'
import ItemList from '@/views/ItemList.vue'
import Item from '@/components/Item.vue'
import { fetchItems } from '@/api/api'
jest.mock('@/api/api.js')

describe('ItemList.vue', () => {
  it('renders an Item for each item returned by fetchItems', async () => {
    const $bar = {
      start: () => {},
      finish: () => {}
    }
    const items = [ {}, {} ]
    fetchItems.mockImplementation(() => Promise.resolve(items))
    const wrapper = shallowMount(ItemList, {mocks: {$bar}})
    await flushPromises()
    expect(wrapper.findAll(Item).length).toEqual(items.length)
  })

  it('passes an item object to each Item component', async () => {
    const $bar = {
      start: () => {},
      finish: () => {}
    }
    const items = [{ id: 1 }, { id: 2 }, { id: 3 }]
    fetchItems.mockImplementation(() => Promise.resolve(items))
    const wrapper = shallowMount(ItemList, {mocks: {$bar}})
    await flushPromises()
    const Items = wrapper.findAll(Item)
    Items.wrappers.forEach((wrapper, i) => {
      expect(wrapper.vm.item).toBe(items[i])
    })
  })

  it('calls $bar start on load', () => {
    const $bar = {
      start: jest.fn(),
      finish: () => {}
    }
    shallowMount(ItemList, {mocks: {$bar}})
    expect($bar.start).toHaveBeenCalled()
  })

  it('calls $bar.fail when load unsuccesful', async () => {
    const $bar = {
      start: () => {},
      fail: jest.fn()
    }
    fetchItems.mockImplementation(() => Promise.reject())
    shallowMount(ItemList, {mocks: {$bar}})
    await flushPromises()

    expect($bar.fail).toHaveBeenCalled()
  })

  it('calls $bar.finish when load succesful', async () => {
    const $bar = {
      start: () => {},
      finish: jest.fn()
    }
    fetchItems.mockImplementation(() => Promise.resolve())
    shallowMount(ItemList, {mocks: {$bar}})
    await flushPromises()

    expect($bar.finish).toHaveBeenCalled()
  })

})
