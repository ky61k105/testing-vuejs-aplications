import { shallowMount } from '@vue/test-utils'
import Form from '@/components/Form.vue'

describe('Form component', () => {
  it('should emit "form-submitted" when button is clicked', () => {
    const wrapper = shallowMount(Form, {
      mocks: {
        axios: { post: jest.fn() }
      }
    })
    wrapper.find('button').trigger('submit')
    expect(wrapper.emitted('form-submitted')).toHaveLength(1)
  })

  it('sends post request with email on submit', () => {
    const axios = {
      post: jest.fn()
    }
    const wrapper = shallowMount(Form, {
      mocks: {
        axios
      }
    })
    const input = wrapper.find('input[type="email"]')
    input.element.value = 'email@gmail.com'
    input.trigger('input')
    wrapper.find('button').trigger('submit')
    const url = 'http://demo7437963.mockable.io/validate'
    expect(axios.post.mock.calls[0][0]).toBe(url)
    expect(axios.post.mock.calls[0][1].email).toBe('email@gmail.com')
  })

  it('sends post request with the shouldContact tickbox on submit', () => {
    const axios = {
      post: jest.fn()
    }
    const wrapper = shallowMount(Form, {
      mocks: {
        axios
      }
    })
    wrapper.find('button').trigger('submit')
    const url = 'http://demo7437963.mockable.io/validate'
    expect(axios.post.mock.calls[0][0]).toBe(url)
    expect(axios.post.mock.calls[0][1].enterCompetition).toBe(true)

    const noRadiobox = wrapper.find('input[value="no"]')
    noRadiobox.checked = true
    noRadiobox.trigger('change')
    wrapper.find('button').trigger('submit')

    expect(axios.post.mock.calls[1][0]).toBe(url)
    expect(axios.post.mock.calls[1][1].enterCompetition).toBe(false)
  })
})
