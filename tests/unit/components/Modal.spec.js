import { shallowMount } from '@vue/test-utils'
import Modal from '@/components/Modal.vue'

describe('Modal component', () => {
  it('should emit "close-modal" when button is clicked', () => {
    const wrapper = shallowMount(Modal)
    wrapper.find('button').trigger('click')
    expect(wrapper.emitted('close-modal')).toHaveLength(1)
  })

  it('should render default slot', () => {
    const wrapper = shallowMount(Modal, {
      slots: {
        default: 'test test'
      }
    })
    expect(wrapper.text()).toContain('test test')
  })

  it('should emit "close-modal" when clicked outside modal', () => {
    const wrapper = shallowMount(Modal)
    wrapper.find('.modal__wrapper').trigger('click')
    expect(wrapper.emitted('close-modal')).toHaveLength(1)
  })
})
