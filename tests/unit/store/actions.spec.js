import flushPromises from 'flush-promises'

import actions from '@/store/actions'
import { fetchItems } from '@/api/api'

jest.mock('@/api/api.js')

describe('actions ', () => {
  it('fetchItems should calls commit with setItems and the result of fetchItems', async () => {
    const items = [{}, {}]
    const type = 'top'
    fetchItems.mockImplementation(calledWith => {
      return calledWith === type
        ? Promise.resolve(items)
        : Promise.resolve()
    })
    const context = {
      commit: jest.fn()
    }

    actions.loadItems(context, { type })
    await flushPromises()
    expect(context.commit).toHaveBeenCalledWith('setItems', { items })
  })
})
