import getters from '@/store/getters'

describe('getters', () => {
  it('setItems sets state.items to items', () => {
    const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
    const state = {
      items: [...items]
    }
    const res = getters.displayedItems(state)
    expect(res.length).toBe(20)

    res.forEach((item, i) => {
      expect(item).toEqual(items[i])
    })
  })
})
