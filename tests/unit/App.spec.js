import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'
import Modal from '@/components/Modal.vue'

describe('App component', () => {
  it('Should be visible Modal by default', () => {
    const wrapper = shallowMount(App)
    expect(wrapper.find(Modal).exists()).toBe(true)
  })

  it('Should hide Modal when close-modal is emitted by Modal', () => {
    const wrapper = shallowMount(App)
    wrapper.find(Modal).vm.$emit('close-modal')
    expect(wrapper.find(Modal).exists()).toBe(false)
  })
})
