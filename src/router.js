import Vue from 'vue'
import Router from 'vue-router'
import ItemList from './views/ItemList.vue'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/top/:page?', component: ItemList, props: {type: 'top'} },
    { path: '/', redirect: '/top' }
  ]
})
