export default {
  displayedItems (state) {
    return state.items.slice(0, 20)
  }
}
