import { fetchItems } from '@/api/api'
export default {
  loadItems: async ({ commit, state, getters }, { type }) => {
    return fetchItems(type).then(items => commit('setItems', { items }))
  }
}
