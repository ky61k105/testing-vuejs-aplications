import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

Vue.use(Vuex)

export const storeConfig = {
  state: {
    items: []
  },
  mutations,
  getters,
  actions
}

export default new Vuex.Store(storeConfig)
