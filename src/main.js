import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ProgressBar from '@/components/ProgressBar.vue'
import '@/assets/styles/index.scss'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)

const bar = new Vue(ProgressBar).$mount()
Vue.prototype.$bar = bar
document.body.appendChild(bar.$el)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
